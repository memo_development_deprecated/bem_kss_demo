[TOC]

# BEM/KSS Bastelkasten

## Voraussetzung
- Ruby implementation von SASS
- Node

Quelle ist die [Offizielle Seite](https://nodejs.org/en/download/) oder als Paket wie Beispielsweise über [Brew](https://brew.sh/index_de) _(Prost...)_.

Danach ist das Node-Paket _grunt-cli_ global installiert sein. Dies sollte nach dem Installieren der Node-Pakete der Fall sein. Falls aber die Userberechtigungen nicht stimmen, bzw. das Paket über _sudo_ installiert wurde, kann es zu fehlern kommen.

`sudo chown -R $USER /pfad/zu/den/node/modulen` sollte die Userrechte korrekt setzen.

Die SASS-Implementierung kann mit `gem install sass` installiert werden. Falls die Benutzerrechte Fehlen mit `sudo gem install sass`.  
Die Dart Sass Implementierung – __der Standard__ – ist mit dem Mac am einfachsten über `brew` installierbar: `brew install sass/sass/sass`.

## Kurzinfo zu Node
__WTF?__
Node ist zwar eine Laufzeitumgebung für Javascript-Code, aber (für mich) hauptsächlich interessant wegen dem _NPM (Node Package Manager)_, welcher viele Pakete für die (lokale) Entwicklung bereitstellt.

__Node Pakete installieren__
Node-Pakete können ähnlich wie Composer-Pakete installiert werden.

`npm install` Installiert alle nötigen Pakete innerhalb des aktuellen Verzeichnisses solange ein _package.json_-File mit den nötigen Abhängigkeiten existiert.

Zusätzlichen Pakete können über `npm install paketname --save` installiert werden. Mit dem Argument `--save` werden diese als Abhängigkeit im _package.json_ hinterlegt.

## Wie bringe ich den Scheiss zum Laufen?
Mit viel Liebe und Zuneigung. Falls dies im ersten Anlauf nicht funktioniert, kann die alternative Methode über den Befehl: `npm install` genutzt werden.
Wenn keine Fehlermeldungen auftauchen (siehe oben) sollten die nötigen Pakete installiert sein.

Mit dem Befehl `grunt` werden folgende Tasks ausgeführt:

- Start eines einfachen Servers für die lokale Entwicklung
- Beobachten aller SCSS Dateien im Ordner _scss_ und kompilieren des CSS.
- Beobachten aller SCSS Dateien im Ordner _scss_ und kompilieren des Styleguides.
- Beobachten aller CSS-, JS- und HTML-Dateien und Browserreload.
- Beobachten vom File _scss/homepage.md_ und kompilieren des Styleguides.

__Wichtig zu wissen:__ Der Styleguide wird im Ordner _styleguide_ generiert nachdem eine Änderung an einer _SCSS-Datei_ vorgenommen wurde.

Prost
