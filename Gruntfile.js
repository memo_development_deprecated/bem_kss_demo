'use strict';
const sass = require('node-sass');


module.exports = function(grunt) {
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		'http-server': {
			page: {
				port: 8080,
				cache: 0,
				showDir : false,
// 				autoIndex: true,
				ext: "html",
				runInBackground: false,
				logFn: function(req, res, error) { },
				openBrowser : true,
			}
		},
// 		run: {
// 			server: {
// 				cmd: './node_modules/.bin/http-server',
// 				args: [
// 					'--silent'
// 				]
// 			},
		sass: {
			prod: {
				files: {
					'css/base.css': 'scss/base.scss',
					'styleguide/css/base.css': 'scss/base.scss',
				}
			},
			dev: {
				files: {
					'css/base.css': 'scss/base.scss',
					'styleguide/css/base.css': 'scss/base.scss',
				},
				options: {
					sourcemap: 'inline',
					debugInfo: true,
					style: 'compressed',
				}
			},
			styleguide: {
				files: {
					'css/base.css': 'scss/base.scss',
				},
				options: {
					sourcemap: 'inline',
					debugInfo: true,
				}
			}
		},
		kss: {
			options: {
				title: "Demo Styleguide",
				cache: 0,
				verbose: false,
				css: '../css/base.css',
// 				builder: "node_modules/michelangelo/kss_styleguide/custom-template/",
				js: [
					'../lib/modernizr/modernizr-3.7.1.min.js',
					'../lib/jquery/jquery-3.4.1.min.js',
					'//127.0.0.1:1337/livereload.js',
					'../js/scripts.js',
				]
			},
			prod: {
				css: '../css/base.css',
				src: ['scss/'],
				dest: 'styleguide/'
			}
		},
		watch: {
			livereload: {
				options: {
					livereload: 1337,
					host: '127.0.0.1',
				},
				files: ['**/*.html','**/*.css','**/*.js'],
			},
			prodSass: {
				files: ['scss/**/*.scss'],
				tasks: ['sass:prod','kss:prod']
			},
			devSass: {
				files: ['scss/**/*.scss'],
				tasks: ['sass:dev','kss:prod'],
			},
			styleguideHomepage: {
				files: ['scss/homepage.md'],
				tasks: ['kss:prod']
			},
		},
		concurrent: {
			options: {
				logConcurrentOutput: true
			},
			prod: {
				tasks: [
					'watch:prodSass'
				]
			},
			dev: {
				tasks: [
					'http-server:page',
					'watch:devSass',
					'watch:styleguideHomepage',
					'watch:livereload',
				]
			},
		}
	});

	grunt.loadNpmTasks('grunt-http-server');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-concurrent');

	grunt.registerTask('default', ['concurrent:dev']);
	grunt.registerTask('dev', ['concurrent:dev']);
	grunt.registerTask('prod', ['sass:prod']);
};
